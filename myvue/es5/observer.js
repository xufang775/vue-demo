var data = {name:'kindeng'};
observe(data);
data.name = 'dmq';   // 监听到值变化了 kindeng --> dmq

setTimeout(function () {
    data.name = 'eeee'
},2000)

function observe(data) {
    if(!data || typeof data !== 'object'){
        return;
    }
    // 取出所有属性遍历
    Object.keys(data).forEach(function (key) {
        defineReactive(data,key,data[key])
    })
}

function defineReactive(data,key,val) {
    var dep = new Dep();
    observe(val);  // 递归 - 监听子属性
    Object.defineProperty(data,key,{
        get:function () {
            // 由于需要在闭包内添加 watcher, 所以通过 Dep定义一个全局target属性，暂存watcher,添加完移除
            Dep.target && dep.addSub(Dep.target);
            return val;
        },
        set:function (newVal) {
            if(val === newVal) return;
            console.log('哈哈，监听到值变化了',val,'-->', newVal);
            val = newVal;
            dep.notify();  // 通知所有订阅者
        }
    })
}
// 消息订阅器，维护一个数组，用于收集订阅器
function Dep() {
    this.subs = [];
}
Dep.prototype = {
    addSub:function (sub) {
        this.subs.push(sub);
    },
    notify:function () {
        this.subs.forEach(function (sub) {
            sub.update();
        })
    }
}
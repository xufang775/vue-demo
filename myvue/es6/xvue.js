// 期待用法
// new XVue({
//     data:{msg:'hello'}
// })

class XVue{
    constructor(options){
        this.$options = options;
        
        // 处理 data 选项
        this.$data = options.data;
        // 响应化
        this.observe(this.$data);

        // new Watcher();
        // this.$data.test;
        // new Watcher();
        // this.$data.foo.bar;

        // this --> vue 实例
        new Compile(options.el,this);

        if(options.created){
            options.created.call(this)
        }
    }
    
    observe(value){
        if(!value || typeof value !== 'object'){
            return;
        }
        // 遍历对象
        Object.keys(value).forEach(key => {
            this.defineReactive(value,key,value[key])
            // 代理到 vm上
            this.proxyData(key);
        })
    }
    proxyData(key){
        Object.defineProperty(this,key,{
            get(){
                return this.$data[key]
            },
            set(newVal){
                this.$data[key] = newVal;
            }
        })
    }
    
    defineReactive(obj,key,val){
        const dep = new Dep();
        Object.defineProperty(obj,key,{
            get(){
                // 将Dep.target添加到dep中
                Dep.target && dep.addDep(Dep.target)
                return val;
            },
            set(newVal){
                if(newVal !== val){
                    val = newVal;
                    // console.log(`${key}更新了，新值:${newVal}`)
                    dep.notify();
                }
            }
        })
        // 递归
        this.observe(val);
    }
}
/*
依赖：此类管理着一个数据对应的依赖，可能有多个依赖
 */
class Dep{
    constructor(){
        this.deps = [];
    }

    addDep(dep){
        this.deps.push(dep)
    }

    notify(){
        this.deps.forEach(dep=>dep.update())
    }
}

class Watcher{
    constructor(vm,key,cb){
        this.vm = vm;
        this.key = key;
        this.cb = cb;

        Dep.target = this;
        this.vm[this.key];  // 添加watcher 到 dep
        Dep.target = null;
    }

    update(){
        // console.log('属性更新了')
        this.cb.call(this.vm,this.vm[this.key])
    }
}
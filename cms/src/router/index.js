import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Member from '@/components/member/Member'
import Shopcart from '@/components/shopcart/Shopcart'
import Search from '@/components/search/Search'
import NewsList from '@/components/news/NewsList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/member',
      name: 'member',
      component: Member
    },
    {
      path: '/shopcart',
      name: 'shopcart',
      component: Shopcart
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path:'/news/List',
      name:'NewsList',
      component:NewsList
    }
  ]
})

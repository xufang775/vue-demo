// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import router from './router'

// mint-ui 开始
// 引入mint-ui
import MintUI from 'mint-ui'
// 安装插件
// 里面其实做的就是注册所有的全局组件，和给Vue.prototype 挂载一些对象，方便你使用就一样的。
// 组件内的this.xxx就能用了。
Vue.use(MintUI);
// 引入css
import 'mint-ui/lib/style.css'
// mint-ui 结束

// axios 开始
import Axios from 'axios';
// 引入自己的插件安装器
import Installer from '@/plugins/installer';
Vue.use(Installer);
// 给Vue 的原型挂载$axios属性
// Vue.use()
Vue.prototype.$axios = Axios;
Axios.defaults.baseURL = 'http://www.aaa.com:3000/';
//Vue.use(Axios);

// axios 结束

// 全局组件开始
import MyUl from '@/components/common/MyUl';
import MyLi from '@/components/common/MyLi';
import NavBar from '@/components/common/NavBar';

Vue.component(MyUl.name,MyUl);
Vue.component(MyLi.name,MyLi);
Vue.component(NavBar.name,NavBar);
// 全局组件结束

import '../static/css/style.css'

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

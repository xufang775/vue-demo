// es6 Module

// 整个项目的入口文件
import App from './App'
import Vue from '../vue'
import {num1,num2,add} from "./App";

// 整个模块加载
import * as obj from './App'

console.log(num1)
console.log(num2)
add(3,5)

console.log(obj.num1)
console.log(obj.num2)
obj.add(3,5)


new Vue({
    el:'#app',
    components:{
        App:obj.default
    },
    template:`<App></App>`
})
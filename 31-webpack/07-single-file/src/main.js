import Vue from 'vue';
import App from './App.vue'

new Vue({
    el:'#app',
    // vue2.0 新增

    // 使用虚拟DOM 来渲染节点提升性能，因为它是基于js计算
    // 通过使用 createElement(h)来创建dom节点
    // createElement(h) 是render的核心

    // vue编译过程 template 里面的节点解析成虚拟dom
    render:c=>c(App)
    // components:{
    //     App
    // },
    // template:'<App/>'
})
var path = './31-webpack/03-webpack-config/'
module.exports = {
    // 入口文件
    entry:{
        // 可以有多个入口，也可以有一个，如果有一个，就默认从这一个入口开始分析
        "main":path+"main.js"
    },
    output: {
        filename: path+"build.js"
    },
    watch: true,   // 文件监视改动，自动产生build.js,即 自动执行 npm run build03


}
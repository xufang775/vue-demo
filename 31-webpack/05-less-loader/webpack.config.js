var path = require('path');
var pathArr = __dirname.split('\\')
var projectPath = `${pathArr[pathArr.length-2]}/${pathArr[pathArr.length-1]}`
var htmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // 入口文件
    entry:{
        // 可以有多个入口，也可以有一个，如果有一个，就默认从这一个入口开始分析
        "main":"./"+projectPath+"/src/main.js"
    },
    output: {
        path: path.resolve(`./${projectPath}/dist`),
        filename: "build.js"
    },
    watch: true,   // 文件监视改动，自动产生build.js,即 自动执行 npm run build03
    // 声明模块
    // 包含各个loader
    module:{
        loaders: [
            {
                test:/\.css$/,
                loader:'style-loader!css-loader'
            },
            {
                test:/\.(jpg|png|jpeg|gif|svg)$/,
                loader:'url-loader?limit = 60000'
            },
            {
                test:/\.less$/,
                loader:'style-loader!css-loader!less-loader'
            }

        ]
    },
    plugins: [
        new htmlWebpackPlugin({
            template:`./${projectPath}/src/index.html`,  // 参照物
        })
    ]

}
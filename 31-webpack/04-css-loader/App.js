import imgSrc from './mygrid.jpg'

var app = {
    data:function () {
        return {
            imgSrc
        }
    },
    template:`
        <div>
            <p>我的第一个组件</p>
             <img :src="imgSrc">
        </div>
    `
}

export default app;
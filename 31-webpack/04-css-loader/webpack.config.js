var pathArr = __dirname.split('\\')
var path = `./${pathArr[pathArr.length-2]}/${pathArr[pathArr.length-1]}/`

console.log(__dirname)  // E:\学习视频\vue-code\vue-example\31-webpack\04-css-loader
module.exports = {
    // 入口文件
    entry:{
        // 可以有多个入口，也可以有一个，如果有一个，就默认从这一个入口开始分析
        "main":path+"main.js"
    },
    output: {
        filename: path+"build.js"
    },
    watch: true,   // 文件监视改动，自动产生build.js,即 自动执行 npm run build03
    // 声明模块
    // 包含各个loader
    module:{
        loaders: [
            {
                test:/\.css$/,
                loader:'style-loader!css-loader'
            },
            {
                test:/\.(jpg|png|jpeg|gif|svg)$/,
                loader:'url-loader?limit = 50000'
            }
        ]
    }

}
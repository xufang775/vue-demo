var koa = require('koa');
var bodyParser = require('koa-bodyparser');
var router = require('koa-router')();

var app = new koa();
// 跨域设置
app.use(bodyParser());
app.use(async (ctx,next)=>{
    ctx.set('Access-Control-Allow-Origin','*')
    await next();
})

app.use(async (ctx, next)=> {
    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    if (ctx.method == 'OPTIONS') {
        ctx.body = 200;
    } else {
        await next();
    }
});
// 打印参数中间件
app.use(async (ctx,next)=>{
    console.log(ctx.request.body);
    // console.log(ctx.query)
    await next()
})

router.get('/',async function (ctx) {
    ctx.body ={
        token:'jilei',
        msg:'ok'
    }
});

router.get('/str',async (ctx)=>{
    ctx.body="str test";
})
router.post('/add',async (ctx)=>{
    
    ctx.body = 'add success'
})


app.use(router.routes())
app.use(router.allowedMethods())


app.listen('3000',function () {
    console.log('start 127.0.0.1:3000');
})
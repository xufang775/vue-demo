// 数据库操作
var MongoClient = require('mongodb').MongoClient;
var DbUrl = "mongodb://127.0.0.1:27017";
var DbName = "productmanage";
var ObjectID = require('mongodb').ObjectID;
function __connectDb(callback) {
    MongoClient.connect(DbUrl,function (err,client) {
        if(err){
            console.log('数据库连接失败')
        }
        var db = client.db(DbName);

        // 增加 修改 删除
        callback(db)
    })
}

// 数据库查找
/*
用法：
Db.find('user',{},function(){

})
 */
exports.find = function (collectionName,json,callback) {
    __connectDb(function (db) {
        var result =  db.collection(collectionName).find(json);
        result.toArray(function (err,data) {

            callback(err,data);  // 拿到数据执行回调函数
        })
    })
}
// 增加数据
exports.insert = function (collectionName,json,callback) {
    __connectDb(function (db) {
        db.collection(collectionName).insertOne(json,function (err,data) {
            callback(err,data)
        })
    })
}

// 更新数据
exports.update = function (collectionName,json1,json2,callback) {
    __connectDb(function (db) {
        db.collection(collectionName).updateOne(json1,{$set:json2},function (err,data) {
            callback(err,data)
        })
    })
}


// 修改数据
exports.delete = function (collectionName,json,callback) {
    __connectDb(function (db) {
        db.collection(collectionName).deleteOne(json,function (err,data) {
            callback(err,data)
        })
    })
}
exports.ObjectID = ObjectID;
var express = require('express');
var router = express.Router();

router.get('/',function (req,res) {
    res.send('index')
})

router.get('/product',function (req,res) {
   res.send('product页面')
});


router.get('/logout',function (req,res) {
    req.session.destroy(function (err) {
        if (err) {
            console.log(err)
        } else{
            res.redirect('/admin/login')
        }

    });
})

module.exports = router;
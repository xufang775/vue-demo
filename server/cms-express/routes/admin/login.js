var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var md5 = require('md5-node');
var DB = require('../../modules/db');

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({extended:false}));
// parse application/json
router.use(bodyParser.json());


router.get('/',function (req,res) {
    // res.send('登录页面')
    res.render('admin/login')
})

router.post('/doLogin',function (req,res) {
    // res.send('admin user')

    var data = req.body;
    data.password = md5(data.password);
    // 获取数据
    DB.find('user',data,function (err,data) {
        if (data.length > 0) {
            console.log('登录成功')

            // 保存用户信息
            req.session.userinfo = data[0];

            res.redirect('/admin/product');
        } else {
            console.log('登录失败')
            res.send("<script>alert('登录失败！');location.href='/login';</script>")
        }
    })
})


module.exports = router;
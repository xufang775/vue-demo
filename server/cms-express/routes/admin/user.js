var express = require('express');
var router = express.Router();

router.get('/',function (req,res) {
    res.send('admin index.ejs')
})

router.get('/add',function (req,res) {
    res.send('admin user')
})

router.get('/edit',function (req,res) {
    res.send('admin product')
})

module.exports = router;
var express = require('express');
var router = express.Router();
var DB = require('../../modules/db');
var multiparty = require('multiparty');
var fs = require('fs');


router.get('/',function (req,res) {
    // res.send('显示商品首页')

    // 链接数据库，查询数据
    DB.find('product',{},function (err,data) {
        // console.log(data);
        res.render('admin/product/index.ejs',{
            list:data
        })
    })
})

router.get('/add',function (req,res) {
    // res.send('admin user')
    res.render('admin/product/add')
})
router.post('/doAdd',function (req,res) {
    var form = new multiparty.Form();
    form.uploadDir = 'upload';   // 上传图片保存的地址    目录必须存在
    form.parse(req,function (err,fields,files) {
        console.log(fields);
        console.log(files)

        var title = fields.title[0];
        var price = fields.price[0];
        var fee = fields.fee[0];
        var description = fields.description[0];
        var pic = files.pic[0].path;

        DB.insert('product',{
            title,price,fee,description,pic
        },function (err,data) {
            if (!err) {
                res.redirect('/admin/product');  // 上传成功，跳转到首页
            }
        })
    });
});

router.get('/edit',function (req,res) {
    let id = req.query.id;
    DB.find('product',{_id:new DB.ObjectID(id)},function (err,data) {
        console.log(data);
        res.render('admin/product/edit',{
            model:data[0]
        })
    })
})
router.post('/doEdit',function (req,res) {
    var form = new multiparty.Form();
    form.uploadDir = 'upload';   // 上传图片保存的地址    目录必须存在
    form.parse(req,function (err,fields,files) {
        console.log(fields);
        console.log(files)
        var id = fields._id[0];
        var title = fields.title[0];
        var price = fields.price[0];
        var fee = fields.fee[0];
        var description = fields.description[0]

        let postData = {title,price,fee,description};
        if(files.pic[0].originalFilename){
            // 有上传
            postData.pic = files.pic[0].path;
        } else {
            // 没有上传图片，删除插件上传的空文件
            fs.unlink(form.uploadDir + '/'+files[0].pic);
        }

        DB.update('product',{_id:new DB.ObjectID(id)},postData,function (err,data) {
            if (!err) {
                res.redirect('/admin/product');  // 上传成功，跳转到首页
            }
        })
    })
});


router.get('/delete',function (req,res) {
    var id = req.query.id;
    DB.delete('product',{_id:new DB.ObjectID(id)},function (err,data) {
        if (!err) {
            res.redirect('/admin/product');
        }
    })
})

module.exports = router;
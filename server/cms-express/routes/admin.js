var express = require('express');
// 可使用 express.Router 类创建模块化，可挂载的路由句柄
var router = express.Router();

// 后台的路由 所有的后台处理都要经过这里
var login = require('./admin/login');
var product = require('./admin/product');
var user = require('./admin/user');




// 自定义中间件，判断登录状态
router.use(function (req,res,next) {
    if(req.url === '/login' || req.url == '/login/doLogin'){
        next();
    } else {
        if(req.session.userinfo && req.session.userinfo.username != ''){
            console.log(req.session.userinfo);
            // app.locals['userinfo'] = req.session.userinfo;
            req.app.locals['userinfo'] = req.session.userinfo;
            next();
        } else {
            res.redirect('/admin/login');
        }
    }
})


router.use('/login',login);
router.use('/product',product);
router.use('/user',user);

module.exports = router;